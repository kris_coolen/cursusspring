package be.kriscoolen.opdracht14.mocks;

import be.kriscoolen.opdracht13.services.GardeningTool;

public class GardenToolMock implements GardeningTool {
    boolean gardenCalled = false;
    @Override
    public void doGardenJob() {
        gardenCalled = true;
    }

    public boolean isGardenCalled(){
        return gardenCalled;
    }
}
