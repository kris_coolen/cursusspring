package be.kriscoolen.opdracht14.mocks;

import be.kriscoolen.opdracht13.services.CleaningTool;

public class CleaningToolMock implements CleaningTool {
    private boolean cleanCalled = false;

    @Override
    public void doCleanJob() {
        cleanCalled = true;
    }

    public boolean isCalled(){
        return cleanCalled;
    }
}
