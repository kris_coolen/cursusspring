package be.kriscoolen.opdracht14.configs;

import be.kriscoolen.opdracht13.services.GardenServiceImpl;
import be.kriscoolen.opdracht13.services.GardeningService;
import be.kriscoolen.opdracht13.services.GardeningTool;
import be.kriscoolen.opdracht13.services.LoggerFactory;
import be.kriscoolen.opdracht14.mocks.GardenToolMock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@Import(LoggerFactory.class)
public class GardenServiceTestConfig {

    @Bean
    GardeningService testGardener(){
        return new GardenServiceImpl();
    }

    @Bean
    GardeningTool gardeningToolMock(){
        return new GardenToolMock();
    }

}
