package be.kriscoolen.opdracht14.configs;

import be.kriscoolen.opdracht13.services.LoggerFactory;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("classpath:application.properties")
@Import(LoggerFactory.class)
@ComponentScan(basePackages = {"be.kriscoolen.opdracht13.services","be.kriscoolen.opdracht13.aspects"})
@EnableAspectJAutoProxy(exposeProxy = true)
public class DomesticServiceTestConfig {
}
