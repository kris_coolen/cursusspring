package be.kriscoolen.opdracht14.tests;

import be.kriscoolen.opdracht13.services.CleaningService;
import be.kriscoolen.opdracht13.services.CleaningTool;
import be.kriscoolen.opdracht14.configs.CleaningServiceTestConfig;
import be.kriscoolen.opdracht14.mocks.CleaningToolMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(CleaningServiceTestConfig.class)
public class CleaningServiceSpringTest {

    @Autowired
    private CleaningService testCleaner;

    @Autowired
    private CleaningTool mock;

    @Test
    public void testDoJob(){
        testCleaner.clean();
        Assertions.assertTrue(((CleaningToolMock)mock).isCalled());
    }
}
