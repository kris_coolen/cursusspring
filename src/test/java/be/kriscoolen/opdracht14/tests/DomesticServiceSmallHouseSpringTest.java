package be.kriscoolen.opdracht14.tests;

import be.kriscoolen.opdracht13.services.CleaningService;
import be.kriscoolen.opdracht13.services.DomesticService;
import be.kriscoolen.opdracht13.services.DomesticServiceImpl;
import be.kriscoolen.opdracht13.services.GardeningService;
import be.kriscoolen.opdracht14.configs.DomesticServiceTestConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(DomesticServiceTestConfig.class)
@ActiveProfiles("smallHouse")
public class DomesticServiceSmallHouseSpringTest {

    @Autowired
    DomesticService domesticService;

    @Autowired
    GardeningService gardeningService;

    @Autowired
    CleaningService cleaningService;

    @Test
    public void domesticServiceExistsTest(){
        Assertions.assertNotNull(domesticService);
    }

    @Test
    public void gardeningServiceExistsTest(){
        Assertions.assertNotNull(gardeningService);
    }

    @Test
    public void cleaningServiceExistsTest(){
        Assertions.assertNotNull(cleaningService);
    }



}
