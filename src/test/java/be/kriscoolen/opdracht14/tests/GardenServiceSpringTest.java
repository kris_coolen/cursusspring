package be.kriscoolen.opdracht14.tests;

import be.kriscoolen.opdracht13.services.GardeningService;
import be.kriscoolen.opdracht13.services.GardeningTool;
import be.kriscoolen.opdracht14.configs.GardenServiceTestConfig;
import be.kriscoolen.opdracht14.mocks.GardenToolMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(GardenServiceTestConfig.class)
public class GardenServiceSpringTest {

    @Autowired
    GardeningService testGardener;

    @Autowired
    GardeningTool gardeningToolMock;

    @Test
    public void testDoGardenJob(){
        testGardener.garden();
        Assertions.assertTrue(((GardenToolMock) gardeningToolMock).isGardenCalled() );
    }
}
