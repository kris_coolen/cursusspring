package be.kriscoolen.opdracht15.configs;

import be.kriscoolen.opdracht15.services.CleaningService;
import be.kriscoolen.opdracht15.services.CleaningServiceImpl;
import be.kriscoolen.opdracht15.services.CleaningTool;
import be.kriscoolen.opdracht15.services.LoggerFactory;
import be.kriscoolen.opdracht15.mocks.CleaningToolMock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@Import(LoggerFactory.class)
public class CleaningServiceTestConfig {

    @Bean
    public CleaningTool cleaningToolMock(){
        return new CleaningToolMock();
    }

    @Bean
    public CleaningService testCleaner(){
        return new CleaningServiceImpl();
    }
}
