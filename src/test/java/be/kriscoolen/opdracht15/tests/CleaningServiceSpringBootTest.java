package be.kriscoolen.opdracht15.tests;

import be.kriscoolen.opdracht15.services.CleaningService;
import be.kriscoolen.opdracht15.services.CleaningTool;
import be.kriscoolen.opdracht15.configs.CleaningServiceTestConfig;
import be.kriscoolen.opdracht15.mocks.CleaningToolMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;


@SpringBootTest(classes = CleaningServiceTestConfig.class)
public class CleaningServiceSpringBootTest {

    @Autowired
    private CleaningService testCleaner;

    @Autowired
    private CleaningTool cleaningToolMock;

    @Test
    @DirtiesContext
    public void testDoJob(){
        testCleaner.clean();
        Assertions.assertTrue(((CleaningToolMock)cleaningToolMock).isCalled());
    }
}
