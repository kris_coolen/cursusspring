package be.kriscoolen.opdracht7;

import be.kriscoolen.opdracht7.services.DomesticService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class HouseApp {

    public static void main(String[] args) {
        System.out.println("Choose your house profile:");
        System.out.println("1) smallhouse");
        System.out.println("2) bighouse");
        Scanner keyb = new Scanner(System.in);
        System.out.println("Your choice:");
        int choice = keyb.nextInt();
        String profileName = null;
        switch (choice){
            case 1: profileName="smallHouse"; break;
            case 2: profileName="bigHouse"; break;
        }
        try(AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext()){
            ctx.getEnvironment().setActiveProfiles(profileName);
            ctx.register(AppConfig.class);
            ctx.refresh();
            DomesticService service = ctx.getBean("domesticService", DomesticService.class);
            service.runHouseHold();
        }
    }
}
