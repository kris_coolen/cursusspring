package be.kriscoolen.opdracht7.services;

public interface CleaningTool {
    public void doCleanJob();
}
