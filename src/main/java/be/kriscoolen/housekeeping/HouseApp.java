package be.kriscoolen.housekeeping;

import be.kriscoolen.housekeeping.config.AppConfig;
import be.kriscoolen.housekeeping.services.*;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class HouseApp {
    /*//opdracht 1
    public static void main(String[] args) {
        CleaningTool broom = new Broom();
        CleaningTool vacuum = new VacuumCleaner();
        CleaningTool sponge = new Sponge();

        CleaningServiceImpl jill = new CleaningServiceImpl();
        jill.setCleaningTool(broom);
        CleaningServiceImpl bob = new CleaningServiceImpl();
        bob.setCleaningTool(vacuum);
        CleaningServiceImpl jane = new CleaningServiceImpl();
        jane.setCleaningTool(sponge);

        jill.clean();
        bob.clean();
        jane.clean();
    }*/

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
       /* CleaningService jill = ctx.getBean("jill",CleaningService.class);
        CleaningService bob = ctx.getBean("bob",CleaningService.class);
        CleaningService jane = ctx.getBean("jane",CleaningService.class);
        jill.clean();
        bob.clean();
        jane.clean();*/
        CleaningService scott = ctx.getBean("Scott",CleaningService.class);
        System.out.println("start clean 1");
        scott.clean();
        System.out.println("start clean 2");
        scott.clean();

        DisposableDuster duster = ctx.getBean("duster2",DisposableDuster.class);
        duster.doCleanJob();

       // CleaningService omar = ctx.getBean("Omar",CleaningService.class);
        //((CleaningServiceImpl)omar).cleanWithDweil();
        //((CleaningServiceImpl)omar).cleanWithDweil();

        ctx.close();








        ctx.close();
    }
}
