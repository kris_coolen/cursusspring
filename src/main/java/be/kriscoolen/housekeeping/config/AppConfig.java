package be.kriscoolen.housekeeping.config;

import be.kriscoolen.housekeeping.services.*;
import org.springframework.context.annotation.*;

@Configuration
public class AppConfig {

    @Lazy
    @Primary
    @Bean
   // @Scope(value="prototype")
    public CleaningTool broom(){
        return new Broom();
    }

    @Lazy
    @Bean
   // @Scope(value="prototype")
    public CleaningTool vacuum(){
        return new VacuumCleaner();
    }

    @Lazy
    @Bean
   //@Scope(value="prototype")
    public CleaningTool sponge(){
        return new Sponge();
    }

    @Lazy
    @Bean
    public GardeningTool lawnMower(){
        return new LawnMower();
    }

    /*@Bean
    @Primary
    @Scope(value = "prototype",proxyMode = ScopedProxyMode.INTERFACES)
    public CleaningTool duster(){
        return new DisposableDuster();
    }*/


    @Lazy
    @Bean
    @Scope(value="prototype",proxyMode = ScopedProxyMode.TARGET_CLASS)
    public DisposableDuster duster2(){
        return new DisposableDuster();
    }


    /*@Bean
    @Scope(value="prototype",proxyMode = ScopedProxyMode.DEFAULT)
    public Dweil dweil(){
        return new Dweil();
    }*/


    @Bean(name={"jill","broomService"})
    @Lazy
    public CleaningService jill(){
        System.out.println("creating new cleaning service for jill");
        CleaningServiceImpl cs = new CleaningServiceImpl();
        cs.setCleaningTool(broom());
        return cs;
    }

    @Bean(name={"bob","vacuumService"})
    @Lazy
    public CleaningService bob(){
        System.out.println("creating new cleaning service for bob");
        CleaningServiceImpl cs = new CleaningServiceImpl();
        cs.setCleaningTool(vacuum());
        return cs;
    }


    @Bean(name={"jane","spongeService"})
    @Lazy
    public CleaningService jane(){
        System.out.println("creating new cleaning service for jane");
        CleaningServiceImpl cs = new CleaningServiceImpl();
        cs.setCleaningTool(sponge());
        return cs;
    }

    @Bean(initMethod = "init",destroyMethod = "destroy")
    @Lazy
    public GardeningService scott(GardeningTool tool){
        System.out.println("creating new gardening service for scott");
        GardenServiceImpl gs = new GardenServiceImpl();
        gs.setGardeningTool(tool);
        return gs;
    }



    @Primary
    @Bean(name="Scott")
    @Lazy
    public CleaningService Scott(CleaningTool cleaningTool){
        System.out.println("creating new cleaning service for scott");
        CleaningServiceImpl cs = new CleaningServiceImpl();
        cs.setCleaningTool(cleaningTool);
        return cs;
    }

    @Bean(name="domesticService")
    @Lazy
    public DomesticService Kris(CleaningService cs, GardeningService gs){

        DomesticServiceImpl ds = new DomesticServiceImpl();
        ds.setCleaningService(cs);
        ds.setGardeningService(gs);
        return ds;

    }

  /*  @Bean(name="Omar")
    public CleaningService omar(){
        CleaningServiceImpl cs = new CleaningServiceImpl(dweil());
        return cs;
    }*/


}
