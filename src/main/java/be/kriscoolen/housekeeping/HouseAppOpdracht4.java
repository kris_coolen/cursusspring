package be.kriscoolen.housekeeping;

import be.kriscoolen.housekeeping.services.DomesticService;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import be.kriscoolen.housekeeping.config.AppConfig;
public class HouseAppOpdracht4 {
    public static void main(String[] args) {

        try(ConfigurableApplicationContext ctx=new AnnotationConfigApplicationContext(AppConfig.class)){
            System.out.println("Container initialized");
            DomesticService service = ctx.getBean("domesticService",DomesticService.class);
            service.runHouseHold();
        }

    }
}
