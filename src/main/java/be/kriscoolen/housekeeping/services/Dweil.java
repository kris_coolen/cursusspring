package be.kriscoolen.housekeeping.services;

public class Dweil {
    boolean used = false;

    public Dweil(){
        System.out.println("Dweil constructor called!");
    }
    public void doCleanJob(){
        if(used){
            System.out.println("I'm already used. Please wring me out!");
        } else {
            System.out.println("dweil");
            used = true;
        }
    }

    public void testJob(){
        if(used){
            System.out.println("test dirty");
        }else{
            System.out.println("test clean");
        }

    }
}
