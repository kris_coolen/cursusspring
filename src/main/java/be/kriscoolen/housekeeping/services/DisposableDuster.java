package be.kriscoolen.housekeeping.services;

public class DisposableDuster implements CleaningTool{
    private boolean used = false;

    public DisposableDuster(){
        System.out.println("DisposableDuster constructor called!");
    }

    public void doCleanJob(){
        if(used){
            System.out.println("I'm already used. Please throw me away");
        } else {
            System.out.println("Wipe the dust away");
            used = true;
        }
    }
}
