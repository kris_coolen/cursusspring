package be.kriscoolen.housekeeping.services;

public class Sponge implements CleaningTool {

    public Sponge(){
        System.out.println("Sponge constructor called!");
    }

    @Override
    public void doCleanJob() {
        System.out.println("Wipe wipe");
    }
}
