package be.kriscoolen.housekeeping.services;

public class GardenServiceImpl implements GardeningService {
    private GardeningTool gardeningTool;

    public void setGardeningTool(GardeningTool tool) {
        this.gardeningTool = tool;
    }

    public void init(){
        System.out.println("GardeningService preparing for work.");
    }

    public void destroy(){
        System.out.println("GardeningService cleaning up.");
    }

    @Override
    public void garden() {
        System.out.println("Working in the garden.");
        gardeningTool.doGardenJob();
    }
}
