package be.kriscoolen.housekeeping.services;

public class DomesticServiceImpl implements DomesticService {


    CleaningService cleaningService;
    GardeningService gardeningService;

    public void setCleaningService(CleaningService cleaningService) {
        this.cleaningService = cleaningService;
    }

    public void setGardeningService(GardeningService gardeningService) {
        this.gardeningService = gardeningService;
    }

    @Override
    public void runHouseHold() {
        cleaningService.clean();
        gardeningService.garden();
    }
}
