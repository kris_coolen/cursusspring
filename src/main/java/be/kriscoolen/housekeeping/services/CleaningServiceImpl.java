package be.kriscoolen.housekeeping.services;

public class CleaningServiceImpl implements CleaningService {

    private CleaningTool tool;
    //private Dweil dweil;

    public CleaningServiceImpl(CleaningTool tool){
        this.tool = tool;
    }
    public CleaningServiceImpl(){

    }

    //public CleaningServiceImpl(Dweil dweil){
    //    this.dweil = dweil;
    //}

    public void setCleaningTool(CleaningTool tool){
        this.tool=tool;
    }

    @Override
    public void clean() {
        System.out.println("Cleaning the house");
        tool.doCleanJob();
    }

    //public void cleanWithDweil(){
    //    dweil.doCleanJob();
    //}
}
