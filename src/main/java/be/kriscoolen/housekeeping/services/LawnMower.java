package be.kriscoolen.housekeeping.services;

public class LawnMower implements GardeningTool {

    public LawnMower(){
        System.out.println("LawnMower constructor called!");
    }
    @Override
    public void doGardenJob() {
        System.out.println("Mowing the lawn");
    }
}
