package be.kriscoolen.housekeeping.services;

public interface GardeningTool {

    public void doGardenJob();

}
