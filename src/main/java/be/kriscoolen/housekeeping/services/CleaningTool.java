package be.kriscoolen.housekeeping.services;

public interface CleaningTool {
    public void doCleanJob();
}
