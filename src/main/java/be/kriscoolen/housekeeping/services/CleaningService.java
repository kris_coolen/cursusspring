package be.kriscoolen.housekeeping.services;

public interface CleaningService {
    public void clean();
}
