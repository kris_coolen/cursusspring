package be.kriscoolen.housekeeping.services;

public class Broom implements CleaningTool {

    public Broom(){
        System.out.println("Broom constructor called!");
    }

    @Override
    public void doCleanJob() {
        System.out.println("Scrub scrub");
    }
}
