package be.kriscoolen.housekeeping;

import be.kriscoolen.housekeeping.config.AppConfig;
import be.kriscoolen.housekeeping.services.CleaningService;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class HouseAppOpdracht3 {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        CleaningService jill = ctx.getBean("jill",CleaningService.class);
        CleaningService broomService = ctx.getBean("broomService",CleaningService.class);
//        CleaningService bob = ctx.getBean("bob",CleaningService.class);
//        CleaningService jane = ctx.getBean("jane",CleaningService.class);
//        CleaningService scott = ctx.getBean("Scott",CleaningService.class);
//        jill.clean();
//        bob.clean();
//        jane.clean();
//        scott.clean();
//        scott.clean();
        jill.clean();
        broomService.clean();
    }
}
