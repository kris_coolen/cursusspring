package be.kriscoolen.opdracht10;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {

        try(ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class)){
            Computer pc = ctx.getBean("computer",Computer.class);
            System.out.println(pc);
        }

    }
}
