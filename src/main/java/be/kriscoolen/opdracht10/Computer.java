package be.kriscoolen.opdracht10;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;

@Component
public class Computer {
    private LocalDate purchaseDate;
    private URL vendorURL;
    private double price;
    private String cpu;
    private long memory;
    private int processors;
    private String ownerName;
    private String os;
    private List<String> features;

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    @Value("#{T(java.time.LocalDate).now()}")
    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public URL getVendorURL() {
        return vendorURL;
    }

    @Value("#{new java.net.URL('https://maxict.nl/fujitsu-lifebook-u758-16ghz-i5-8250u-156-1920-x-1080pixels-4g-zwart-notebook-p8951812.html?btwview=in&gclid=Cj0KCQjwncT1BRDhARIsAOQF9LlhpV4faAR_-J1QboI_O9adIYg0LydbCGsBGJIxiMSTfghVkFZygS8aAr9hEALw_wcB&utm_medium=cpc&utm_source=googleshopping')}")
    public void setVendorURL(URL vendorURL) {
        this.vendorURL = vendorURL;
    }


    public double getPrice() {
        return price;
    }

    @Value("#{1750.74}")
    public void setPrice(double price) {
        this.price = price;
    }

    public String getCpu() {
        return cpu;
    }


    @Value("#{systemProperties['os.arch']}")
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public long getMemory() {
        return memory;
    }

    @Value("#{T(Runtime).getRuntime().totalMemory()}")
    public void setMemory(long memory) {
        this.memory = memory;
    }

    public int getProcessors() {
        return processors;
    }

    @Value("#{T(Runtime).getRuntime().availableProcessors()}")
    public void setProcessors(int processors) {
        this.processors = processors;
    }

    public String getOwnerName() {
        return ownerName;
    }

    @Value("#{systemProperties['user.name']}")
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOs() {
        return os;
    }

    @Value("#{systemProperties['os.name']}")
    public void setOs(String os) {
        this.os = os;
    }

    public List<String> getFeatures() {
        return features;
    }

    @Value("#{{'DDR4-SDRAM werkgeheugen','LED backlight','Bluetooth','4G'}}")
    public void setFeatures(List<String> features) {
        this.features = features;
    }

    @Override
    public String toString() {
        return "This Computer:\n" +
                "\tpurchaseDate=" + purchaseDate +
                "\n\tvendorURL=" + vendorURL +
                "\n\tprice=" + price +
                "\n\tcpu='" + cpu + '\'' +
                "\n\tmemory=" + memory +
                "\n\tprocessors=" + processors +
                "\n\townerName='" + ownerName + '\'' +
                "\n\tos='" + os + '\'' +
                "\n\tfeatures=" + features +
                '}';
    }
}
