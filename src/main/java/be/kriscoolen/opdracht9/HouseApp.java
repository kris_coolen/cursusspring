package be.kriscoolen.opdracht9;

import be.kriscoolen.opdracht9.services.DomesticService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HouseApp {
    public static void main(String[] args) {
        try(ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext()){
            ctx.getEnvironment().setActiveProfiles("smallHouse");
            ctx.setConfigLocation("housekeeping.xml");
            ctx.refresh();
            DomesticService service = ctx.getBean("domesticService",DomesticService.class);
            service.runHouseHold();
        }
    }
}
