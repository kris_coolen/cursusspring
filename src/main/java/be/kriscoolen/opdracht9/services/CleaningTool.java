package be.kriscoolen.opdracht9.services;

public interface CleaningTool {
    public void doCleanJob();
}
