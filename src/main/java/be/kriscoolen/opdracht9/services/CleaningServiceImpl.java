package be.kriscoolen.opdracht9.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Component
@Profile("smallHouse")
public class CleaningServiceImpl implements CleaningService {

    private Logger logger;
    private CleaningTool tool;
    private float rate;

    @Autowired
    public CleaningServiceImpl(CleaningTool tool) {
        this.tool = tool;
    }

    private CleaningServiceImpl() {
    }


    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    //@Autowired
    public void setCleaningTool(CleaningTool tool) {
        this.tool = tool;
    }

    @Value("${rate}")
    public void setRate(float rate) {
        this.rate = rate;
    }

    @PostConstruct
    public void init(){
        logger.info("CleaningService preparing for work with rate " + rate);
    }

    @PreDestroy
    public void destroy(){
        logger.info("CleaningService cleaning up");
    }

    @Override
    public void clean() {
        logger.info("Cleaning the house");
        tool.doCleanJob();
    }
}
