package be.kriscoolen.opdracht15;

import be.kriscoolen.opdracht15.services.DomesticService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class HouseApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(HouseApp.class);
        DomesticService service = ctx.getBean("domesticService",DomesticService.class);
        service.runHouseHold();
    }
}
