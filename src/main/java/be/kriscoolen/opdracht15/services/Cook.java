package be.kriscoolen.opdracht15.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
public class Cook {

    @Autowired
    private ApplicationEventPublisher publisher;


    public void makeLunch(){
        System.out.println("Meal ready");
        publisher.publishEvent(new LunchEvent());
    }

    @EventListener(classes = ContextStartedEvent.class)
    @Order(2)
    public void startCooking(){
        System.out.println("Preparing meal");
        makeLunch();
    }
}
