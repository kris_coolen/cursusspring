package be.kriscoolen.opdracht15.services;

public interface CleaningTool {
    public void doCleanJob();
}
