package be.kriscoolen.opdracht15.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Locale;
import java.util.logging.Logger;

@Component("domesticService")
public class DomesticServiceImpl implements DomesticService {

    private Logger logger;
    private CleaningService cleaningService;
    private GardeningService gardeningService;

    private MessageSource messageSource;
    private Locale locale;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Value("#{T(java.util.Locale).getDefault()}")
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Autowired
    public void setLogger(Logger logger){
        this.logger=logger;
    }

    @Autowired
    public void setCleaningService(CleaningService cleaningService) {
        this.cleaningService = cleaningService;
    }

    @Autowired
    public void setGardeningService(GardeningService gardeningService) {
        this.gardeningService = gardeningService;
    }

    @PostConstruct
    public void init(){
        logger.info(messageSource.getMessage("welcome",new Object[]{12},locale));
        logger.info("DomesticService preparing for work.");
    }

    @PreDestroy
    public void destroy(){
        logger.info("DomesticService cleaning up");
    }

    @EventListener(classes = ContextStartedEvent.class)
    @Override
    @Order(1)
    public void runHouseHold() {
        //logger.info("Running household");
        cleaningService.clean();
        gardeningService.garden();
    }
}
