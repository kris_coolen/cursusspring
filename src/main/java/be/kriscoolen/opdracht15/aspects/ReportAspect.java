package be.kriscoolen.opdracht15.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Aspect
public class ReportAspect {

     Logger logger;

     @Autowired
     public void setLogger(Logger logger){
     this.logger=logger;
     }


    @Before("execution(* be.kriscoolen.opdracht13.services.DomesticServiceImpl.runHouseHold(..))")
    public void beforeRunningHouseHold(){
        //logger.info("Running household");
        System.out.println("Started running household");

    }

    @After("execution(* be.kriscoolen.opdracht13.services.DomesticServiceImpl.runHouseHold(..))")
    public void afterRunningHouseHold(){
        System.out.println(("Finished running the household"));
    }

    @AfterThrowing(value="execution(* be.kriscoolen.opdracht13.services.DomesticServiceImpl.runHouseHold(..))",throwing = "ex")
    public void afterExceptionRunningHouseHold(Exception ex){
        System.out.println("Something went wrong: " + ex.getMessage());
    }

   /* @Before("execution(* *.garden(..))")
    public void beforeRunningHouseHold(){
        //logger.info("Running household");
        System.out.println("Running household");

    }*/

}
