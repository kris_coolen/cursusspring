package be.kriscoolen.opdracht5en6;

import be.kriscoolen.opdracht5en6.services.DomesticService;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class HouseApp {
    public static void main(String[] args) {

        try(ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class)){
            System.out.println("Container initialized");
            DomesticService service =ctx.getBean("domesticService",DomesticService.class);
            //GardeningService lawnMowerService = ctx.getBean("myGardeningService2",GardeningService.class);
            //lawnMowerService.garden();
            service.runHouseHold();
        }

    }
}
