package be.kriscoolen.opdracht5en6.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Component("cleaningService")
public class CleaningServiceImpl implements CleaningService {

    private Logger logger;
    private CleaningTool tool;
    private float rate;

    @Autowired
    public CleaningServiceImpl(@Qualifier("wet") CleaningTool tool) {
        this.tool = tool;
    }

    private CleaningServiceImpl() {
    }


    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    //@Autowired
    public void setCleaningTool(@Qualifier("wet") CleaningTool tool) {
        this.tool = tool;
    }

    @Value("9")
    public void setRate(float rate) {
        this.rate = rate;
    }

    @PostConstruct
    public void init(){
        System.out.println("CleaningService preparing for work.");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("CleaningService cleaning up");
    }

    @Override
    public void clean() {
        logger.info("Cleaning the house");
        tool.doCleanJob();
    }
}
