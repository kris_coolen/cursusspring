package be.kriscoolen.opdracht5en6.services;

public interface CleaningTool {
    public void doCleanJob();
}
