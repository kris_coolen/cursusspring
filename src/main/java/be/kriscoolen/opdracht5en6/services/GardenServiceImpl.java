package be.kriscoolen.opdracht5en6.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Component
public class GardenServiceImpl implements GardeningService {
    private GardeningTool gardeningTool;

    private Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Autowired
    public void setGardeningTool(GardeningTool tool) {
        this.gardeningTool = tool;
    }


    @PostConstruct
    public void init(){
        System.out.println("GardeningService preparing for work.");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("GardeningService cleaning up");
    }

    @Override
    public void garden() {
        logger.info("Working in the garden.");
        gardeningTool.doGardenJob();
    }
}
