package be.kriscoolen.opdracht5en6.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.logging.Logger;

@Component
@Qualifier("robot")
public class CleaningRobot implements CleaningService {

    private Logger logger;
    private List<CleaningTool> tools;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Autowired
    public void setCleaningTools(List<CleaningTool> tools){
        this.tools=tools;
    }

    @PostConstruct
    public void init(){
        System.out.println("CleaningRobot preparing for work.");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("CleaningRobot cleaning up");
    }

    @Override
    public void clean() {
        logger.info("Cleaning the house");
        tools.forEach(CleaningTool::doCleanJob);
    }
}
