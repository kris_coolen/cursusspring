package be.kriscoolen.opdracht5en6.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope("prototype")
@Qualifier("electric")
@Order(3)
public class VacuumCleaner implements CleaningTool {

    private Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void doCleanJob() {
        logger.info("Zuuuuuuuuuuuuu");
    }
}
