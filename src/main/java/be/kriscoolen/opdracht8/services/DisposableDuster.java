package be.kriscoolen.opdracht8.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope(value="prototype",proxyMode = ScopedProxyMode.INTERFACES)
@Order(1)
@Profile("bigHouse")
public class DisposableDuster implements CleaningTool {
    private boolean used = false;

    private Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public void doCleanJob(){
        if(used){
            logger.info("I'm already used. Please throw me away");
        } else {
            logger.info("Wipe the dust away");
            used = true;
        }
    }
}
