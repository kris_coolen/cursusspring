package be.kriscoolen.opdracht8.services;

public interface CleaningTool {
    public void doCleanJob();
}
