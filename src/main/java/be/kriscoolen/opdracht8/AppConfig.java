package be.kriscoolen.opdracht8;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(lazyInit = true)
@PropertySource("classpath:application.properties")
public class AppConfig {
}
