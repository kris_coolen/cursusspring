package be.kriscoolen.opdracht13.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Component("domesticService")
public class DomesticServiceImpl implements DomesticService {

    private Logger logger;
    private CleaningService cleaningService;
    private GardeningService gardeningService;

    @Autowired
    public void setLogger(Logger logger){
        this.logger=logger;
    }

    @Autowired
    public void setCleaningService(CleaningService cleaningService) {
        this.cleaningService = cleaningService;
    }

    @Autowired
    public void setGardeningService(GardeningService gardeningService) {
        this.gardeningService = gardeningService;
    }

    @PostConstruct
    public void init(){
        logger.info("DomesticService preparing for work.");
    }

    @PreDestroy
    public void destroy(){
        logger.info("DomesticService cleaning up");
    }

    @Override
    public void runHouseHold() {
        logger.info("Running household");
        cleaningService.clean();
        gardeningService.garden();
    }
}
