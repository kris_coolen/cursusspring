package be.kriscoolen.opdracht13.services;

public interface CleaningTool {
    public void doCleanJob();
}
