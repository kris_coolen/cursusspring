package be.kriscoolen.opdracht13;

import be.kriscoolen.opdracht13.services.DomesticService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class HouseApp {

    public static void main(String[] args) {
        try(AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext()){
            ctx.getEnvironment().setActiveProfiles("smallHouse");
            ctx.register(AppConfig.class);
            ctx.refresh();
            DomesticService service = ctx.getBean("domesticService", DomesticService.class);
            service.runHouseHold();
        }
    }
}
