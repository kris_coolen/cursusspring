package be.kriscoolen.opdracht13;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(lazyInit = true)
@PropertySource("classpath:application.properties")
@EnableAspectJAutoProxy(exposeProxy = true)
public class AppConfig {
}
