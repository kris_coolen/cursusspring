package be.kriscoolen.opdracht11en12en13;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class HouseApp {

    public static void main(String[] args) {
        //Locale.setDefault(new Locale("de"));
        try(AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext()){
            ctx.getEnvironment().setActiveProfiles("smallHouse");
            ctx.register(AppConfig.class);
            ctx.refresh();
            ctx.start();
            //DomesticService service = ctx.getBean("domesticService", DomesticService.class);
            //service.runHouseHold();


            //ctx.publishEvent(new LunchEvent());
        }
    }
}
