package be.kriscoolen.opdracht11en12en13.services;

public interface CleaningTool {
    public void doCleanJob();
}
