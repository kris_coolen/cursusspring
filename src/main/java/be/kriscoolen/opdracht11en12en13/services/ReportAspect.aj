package be.kriscoolen.opdracht11en12en13.services;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Aspect
public class ReportAspect {

   // Logger logger;

   // @Autowired
   // public void setLogger(Logger logger){
       // this.logger=logger;
   // }

    @Before("execution(void be.kriscoolen.opdracht11en12en13.services.DomesticServiceImpl.runHouseHold(void))")
    public void beforeRunningHouseHold(){
        //logger.info("Running household");
        System.out.println("Running household");

    }

    /*@Before("execution(* *.runHouseHold(..))")
    public void beforeRunningHouseHold(){
        //logger.info("Running household");
        System.out.println("Running household");

    }*/

}
