package be.kriscoolen.opdracht11en12en13.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.logging.Logger;

@Component
@Profile("bigHouse")
public class HedgeTrimmerFactory {

    private Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Bean
    public GardeningTool hedgeTrimmer(){
        int hour = LocalTime.now().getHour();
        if(hour>8 && hour <18){
            return ()-> logger.info("Trimming electric");
        }
        else{
            return ()-> logger.info("Trimming manual");
        }
    }
}
